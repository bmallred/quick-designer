(function (window, document) {
  function quickDesignerTypography (selector) {
    var elements;
    if (selector) {
      elements = document.querySelectorAll(selector);
      for (var j = 0; j < elements.length; j++) {
        quickDesignerTypographyByElement(elements[j]);
      }
    } else {
      var queue = [];
      elements = document.evaluate('//*[normalize-space(text()) != ""]', document, null, XPathResult.ANY_TYPE, null);

      try {
        var node = elements.iterateNext();
        while (node) {
          switch (node.tagName) {
          case 'SCRIPT':
          case 'STYLE':
          case 'TITLE':
          case 'I':
            // These elements should not be processed
            break;
          default:
            // Make sure the node is visible
            if (node.style.display !== 'none') {
              queue.push(node);
            }
            break;
          }

          // Move to the next element
          node = elements.iterateNext();
        }
      } catch (e) {
        console.log('Error: DOM modified during iteration... ' + e);
      }

      for (var i = 0; i < queue.length; i++) {
        quickDesignerTypographyByElement(queue[i]);
      }
    }
  }

  function quickDesignerTypographyByElement (el) {
    if (el.dataset.innerHTML) {
      restoreElement(el);
    } else {
      showTypography(el);
    }
  }

  function restoreElement (el) {
    // Retrieve the stored data
    el.innerHTML = decodeURI(el.dataset.innerHTML);
    el.setAttribute('title', el.dataset.title || '');
    el.style.overflow = el.dataset.overflow;
    el.style.textOverflow = el.dataset.textOverflow;
    el.style.whiteSpace = el.dataset.whiteSpace;

    // Delete the saved data from the DOM
    delete el.dataset.innerHTML;
    delete el.dataset.title;
    delete el.dataset.overflow;
    delete el.dataset.textOverflow;
    delete el.dataset.whiteSpace;
  }

  function showTypography (el) {
    // Pull back the computed style from the window for
    // a more accurate picture of what was rendered
    var style = window.getComputedStyle(el);
    var description = '';

    // Only display the first family
    var family = style.fontFamily;
    family = family.replace(/"/gi, '');
    family = trim(family.split(',', 2)[0]);
    description += family;
    description += ', ' + style.fontSize + ' / ' + style.lineHeight;

    // These `normalized` names for the integer values of
    // the weight come from the following resource:
    //
    // https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight
    var weight = style.fontWeight;
    switch (weight) {
    case '100':
      weight = 'thin';
      break;
    case '200':
      weight = 'extra light';
      break;
    case '300':
      weight = 'light';
      break;
    case '400':
      weight = 'normal';
      break;
    case '500':
      weight = 'medium';
      break;
    case '600':
      weight = 'semi-bold';
      break;
    case '700':
      weight = 'bold';
      break;
    case '800':
      weight = 'extra bold';
      break;
    case '900':
      weight = 'heavy';
      break;
    }
    description += ', ' + weight;

    // Store the information in case we need to reset the
    // values
    el.dataset.innerHTML = encodeURI(el.innerHTML);
    el.dataset.title = el.getAttribute('title') || '';
    el.dataset.overflow = el.style.overflow;
    el.dataset.textOverflow = el.style.textOverflow;
    el.dataset.whiteSpace = el.style.whiteSpace;

    // Apply new information
    el.innerText = description;
    el.setAttribute('title', description);
    el.style.overflow = 'hidden';
    el.style.textOverflow = 'ellipsis';
    el.style.whiteSpace = 'nowrap';
  }

  function trim(str) {
    str = str.replace(/^\s\s*/, '');
    var ws = /\s/;
    var i = str.length;
    while (ws.test(str.charAt(--i)));
    return str.slice(0, i + 1);
  }

  if (!window.quickDesigner) {
    window.quickDesigner = {};
  }
  window.quickDesigner.typography = quickDesignerTypography;
  window.quickDesigner.typographyByElement = quickDesignerTypographyByElement;
})(window, document);


