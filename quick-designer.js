(function (window, document) {
  function inject (src, callback) {
    var target = (document.body || document.head || document.documentElement);
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = src;

    var done = false;
    script.onload = script.onreadystatechange = function () {
      if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
        done = true;

        // Continue your code
        callback();

        // Handle memory leak in IE
        script.onload = script.onreadystatechange = null;
        target.removeChild(script);
      }
    }

    target.appendChild(script);
  }
  
  inject('https://gl.githack.com/bmallred/quick-designer/raw/master/quick-typography.js', function () {
    inject('https://gl.githack.com/bmallred/quick-designer/raw/master/quick-palette.js', function () {
      window.quickDesigner.typography();
      window.quickDesigner.palette();
    });
  });
})(window, document);


