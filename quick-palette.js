(function (window, document) {
  function quickDesignerPalette () {
    var palettes = document.querySelectorAll('.quick-designer-palette');
    if (palettes.length) {
      // Remove any previous palettes
      for (var i = 0; i < palettes.length; i++) {
        palettes[i].outerHTML = '';
        delete palettes[i];
      }

      // Toggled off
      return;
    }

    var elements = document.querySelectorAll('*');
    var colors = {};

    for (var i = 0; i < elements.length; i++) {
      var style = window.getComputedStyle(elements[i]);
      colors = countColor(style.color, colors);
      colors = countColor(style.backgroundColor, colors);
    }

    // Sort them by most commonly found first
    colors = Object.keys(colors).sort(function (a, b) {
      return colors[a] - colors[b];
    });

    // Create a new palette
    var stickToRight = 'position: fixed; right: 0; top: 20vh; max-height: 60vh; width: 80px; padding: 1rem; box-shadow: 0 4px 20px #000; background-color: #fff; opacity: 0.9; border-bottom-left-radius: 4px; border-top-left-radius: 4px; overflow-y: auto; z-index: 9999;';
    var stickToBottom = 'position: fixed; bottom: 0; width: 100%; text-align: center; padding: 1rem; box-shadow: 0 4px 20px #000; background-color: #fff; opacity: 0.9; z-index: 9999;';
    var palette = '<div class="quick-designer-palette" style="' + stickToBottom + '">';
    for (var i = 0; i < colors.length; i++) {
      var color = colors[i];
      palette += '<span style="display: inline-block; margin: 0 2px; width: 16px; height: 16px; cursor: pointer; background-color: ' + color + ';" title="' + color + '" onclick="' + copyToClipboard(color) + '">&nbsp;</span>';
    }
    palette += '</div>';
    document.body.innerHTML += palette;
  }

  // Add the color or increment the counter.
  // NOTE: This also converts all RGBA values to RGB for normalcy.
  function countColor (color, colors) {
    color = rgbaToRgb(color);
    
    if (colors[color]) {
      colors[color]++;
    } else {
      colors[color] = 1;
    }

    return colors;
  }
  
  function rgbaToRgb (value) {
    if (value.indexOf('rgba') === -1) {
      return value;
    }

    value = value.replace(/rgba\(/gi, '');
    value = value.replace(/\)/gi, '');

    var parts = value.split(',');
    return 'rgb(' + parts[0] + ', ' + parts[1] + ', ' + parts[2] + ')';
  }

  function copyToClipboard (text) {
    if (window.clipboardData) {
      return 'window.clipboardData.setData("Text", "' + text + '");';
    }

    return "var el = document.createElement('div');" +
      "el.innerText = '" + text + "';" +
      "el.style.position = 'absolute';" +
      "el.style.left = '-1000';" +
      "el.style.top = '-1000';" +
      "document.body.appendChild(el);" +
      "var range = document.createRange();" +
      "range.selectNodeContents(el);" +
      "var sel = window.getSelection();" +
      "sel.removeAllRanges();" +
      "sel.addRange(range);" +
      "document.execCommand('copy');" +
      "document.body.removeChild(el);"
  }

  if (!window.quickDesigner) {
    window.quickDesigner = {};
  }
  window.quickDesigner.palette = quickDesignerPalette;
})(window, document);
